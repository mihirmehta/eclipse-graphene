#!/bin/bash
# ===============LICENSE_START=======================================================
# Graphene Apache-2.0
# ===================================================================================
# Copyright (C) 2017-2018 AT&T Intellectual Property & Tech Mahindra. All rights reserved.
# ===================================================================================
# This Graphene software file is distributed by AT&T and Tech Mahindra
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END=========================================================
#
# What this is: All-in-One deployment of the Graphene platform. FOR TEST PURPOSES
# ONLY.
#
# Prerequisites:
# - Ubuntu Xenial (16.04), Bionic (18.04), or Centos 7 hosts
# - All hostnames specified in graphene_env.sh must be DNS-resolvable on all hosts
#   (entries in /etc/hosts or in an actual DNS server)
# - For deployments behind proxies, set GRAPHENE_HTTP_PROXY and GRAPHENE_HTTPS_PROXY in graphene_env.sh
# - For kubernetes based deployment
#   - Kubernetes cluster deployed
#   - kubectl installed on user's workstation, and configured to use the kube
#     profile for the target cluster, e.g. though setup_kubectl.sh, e.g
#     $ wget https://raw.githubusercontent.com/graphene/kubernetes-client/master/deploy/private/setup_kubectl.sh
#     $ bash setup_kubectl.sh myk8smaster myuser mynamespace
# - Host preparation steps by an admin (or sudo user)
#   - Persistent volume pre-arranged and identified for PV-dependent components
#     in graphene_env.sh. tools/setup_pv.sh may be used for this
#   - the User running this script must have been added to the "docker" group
#     $ sudo usermod <user> -aG docker
#   - AIO prerequisites setup by sudo user via setup_prereqs.sh
#  - If calling this script without first running setup_prereqs.sh, ensure that
#    these values have been set/updated in graphene_env.sh:
#    DEPLOYED_UNDER: k8s|docker
#    GRAPHENE_DOMAIN: DNS or hosts-file resolvable FQDN of the Graphene platform
#    K8S_DIST: openshift|generic
#    GRAPHENE_HOST:  DNS or hosts-file resolvable hostname of the Graphene host
#
# Usage:
#   For docker-based deployments, run this script on the AIO host.
#   For k8s-based deployment, run this script on the AIO host or a workstation
#   connected to the k8s cluster via kubectl (e.g. via tools/setup_kubectl.sh)
#   $ bash oneclick_deploy.sh [aio_k8s_deployer <host> <user> <k8s_dist>]
#     aio_k8s_deployer: (optional) deploy using the aio_k8s_deployer tool
#     host: k8s master host
#     user: SSH-enabled sudo user on the k8s master
#     k8s_dist: k8s distribution (generic|openshift)
#

function stop_graphene_core_in_k8s() {
  trap 'fail' ERR
  if [[ "$GRAPHENE_DEPLOY_FEDERATION" == "true" ]]; then
    apps="cds dsce federation \
      kubernetes-client playground-deployer license-profile-editor license-rtu-editor \
      onboarding portal-be portal-fe sv-scanning docker-proxy"
  else
    apps="cds azure-client deployment-client dsce \
      kubernetes-client msg license-profile-editor license-rtu-editor \
      onboarding portal-be portal-fe sv-scanning docker-proxy"
  fi
  for app in $apps; do
    if [[ $(kubectl delete deployment -n $GRAPHENE_NAMESPACE $app) ]]; then
      log "Deployment deleted for app $app"
    fi
    if [[ $(kubectl delete service -n $GRAPHENE_NAMESPACE $app-service) ]]; then
      log "Service deleted for app $app"
    fi
  done
  cfgs="graphene-certs sv-scanning"
  for cfg in $cfgs; do
    if [[ $(kubectl delete configmap -n $GRAPHENE_NAMESPACE $cfg) ]]; then
      log "Configmap $cfg deleted"
    fi
  done
}

function stop_graphene() {
  trap 'fail' ERR
  if [[ "$GRAPHENE_DEPLOY_MLWB" == "true" ]]; then
    bash mlwb/setup_mlwb.sh clean
  fi
  if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
    log "Stop any running Graphene core docker-based components"
    bash $AIO_ROOT/docker_compose.sh down
  else
    stop_graphene_core_in_k8s
  fi
}

function prepare_env() {
  trap 'fail' ERR
  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    log "Ensure kubectl access to the k8s cluster"
    if ! kubectl get namespace $GRAPHENE_NAMESPACE; then
      kubectl config view
      log 'Unable to access the k8s cluster using kubectl'
      fail 'Verify your kube configuration in ~/.kube/config'
    fi
  fi
  # TODO: redeploy without deleting all services first
  if [[ "$GRAPHENE_DEPLOY_CORE" == "true" ]]; then
    stop_graphene
  fi

  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    log "Create log PVCs in namespace $GRAPHENE_NAMESPACE"
    labels="$GRAPHENE_ACUCOMPOSE_SERVICE_LABEL\n$GRAPHENE_AZURE_CLIENT_SERVICE_LABEL\n\
$GRAPHENE_COMMON_DATA_SERVICE_LABEL\n$GRAPHENE_DEPLOYMENT_CLIENT_SERVICE_LABEL\n\
$GRAPHENE_DOCKER_PROXY_SERVICE_LABEL\n$GRAPHENE_FEDERATION_SERVICE_LABEL\n\
$GRAPHENE_FILEBEAT_SERVICE_LABEL\n$GRAPHENE_KUBERNETES_CLIENT_SERVICE_LABEL\n\
$GRAPHENE_LICENSE_MGT_SERVICE_LABEL\n$GRAPHENE_MICROSERVICE_GENERATION_SERVICE_LABEL\n\
$GRAPHENE_ONBOARDING_SERVICE_LABEL\n$GRAPHENE_PORTAL_SERVICE_LABEL\n\
$GRAPHENE_SECURITY_VERIFICATION_SERVICE_LABEL"
    pvcs=$(printf "$labels" | sort | uniq)
    for pvc in $pvcs; do
      setup_pvc $GRAPHENE_NAMESPACE $pvc $pvc \
        $GRAPHENE_LOGS_PV_SIZE $GRAPHENE_LOGS_PV_CLASSNAME
    done
  fi
}

function setup_graphene() {
    trap 'fail' ERR

    log "Deploy the Graphene core components"
    if [[ ! -e deploy ]]; then mkdir deploy; fi
    if [[ "$GRAPHENE_DEPLOY_FEDERATION" == "true" ]]; then
      apps="cds dsce federation \
        kubernetes-client license-profile-editor license-rtu-editor \
        onboarding portal-be portal-fe playground-deployer"
    else
      apps="cds dsce kubernetes-client license-profile-editor license-rtu-editor \
        onboarding portal-be portal-fe playground-deployer"
    fi
    for app in $apps; do
      start_graphene_core_app $app
    done

}

function add_to_urls() {
  trap 'fail' ERR
  if [[ ! -e graphene.url || $(grep -c "$1" graphene.url) -eq 0 ]]; then
    log "Adding $1: $2 to graphene.url"
    echo "$1: $2" >>graphene.url
  else
    log "Updating graphene.url with $1: $2"
    sedi "s~/$1:.*~/$1: $2~" graphene.url
  fi
}

function setup_ingress_controller() {
  trap 'fail' ERR
  if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
    if [[ "$GRAPHENE_DEPLOY_INGRESS" == "true" ]]; then
      if [[ "$GRAPHENE_INGRESS_SERVICE" == "nginx" ]]; then
        EXTERNAL_IP=""
        if [[ "$GRAPHENE_INGRESS_LOADBALANCER" == "false" ]]; then
          EXTERNAL_IP=$GRAPHENE_DOMAIN_IP
        fi
        bash $AIO_ROOT/../charts/ingress/setup_ingress_controller.sh $GRAPHENE_NAMESPACE \
          $AIO_ROOT/certs/graphene.crt $AIO_ROOT/certs/graphene.key $EXTERNAL_IP
      else
        bash $AIO_ROOT/kong/setup_kong.sh
      fi
    fi
  else
    bash $AIO_ROOT/kong/setup_kong.sh
  fi

  if [[ "$GRAPHENE_PORT" == "" ]]; then
    update_graphene_env GRAPHENE_ORIGIN $GRAPHENE_DOMAIN force
  else
    update_graphene_env GRAPHENE_ORIGIN $GRAPHENE_DOMAIN:$GRAPHENE_PORT force
  fi
  update_graphene_env GRAPHENE_DEPLOY_INGRESS false force
  add_to_urls Portal https://$GRAPHENE_ORIGIN
}

function customize_catalog() {
  local old_name=$1
  local accessTypeCode=$2
  local name=$3
  cds_baseurl="-k https://$GRAPHENE_DOMAIN/ccds"
  check_name_resolves cds-service
  if [[ "$NAME_RESOLVES" == "true" ]]; then
    cds_baseurl="http://cds-service:8000/ccds"
  fi
  local creds="$GRAPHENE_CDS_USER:$GRAPHENE_CDS_PASSWORD"
  local cats=$(curl -s -u $creds $cds_baseurl/catalog | jq '.content | length')
  local cat=0
  while [[ $cat -lt $cats ]]; do
    if [[ "$(curl -s -u $creds $cds_baseurl/catalog | jq -r ".content[$cat].name")" == "$old_name" ]]; then
      local jsonin="/tmp/$(uuidgen)"
      local jsonout="/tmp/$(uuidgen)"
      cid=$(curl -s -u $creds $cds_baseurl/catalog | jq -r ".content[$cat].catalogId")
      cat <<EOF >$jsonin
{
"catalogId": "$cid",
"accessTypeCode": "$accessTypeCode",
"selfPublish": false,
"name": "$name",
"publisher": "$GRAPHENE_DOMAIN",
"description": null,
"origin": null,
"url": "https://$GRAPHENE_ORIGIN"
}
EOF
      curl -s -o $jsonout -u $creds -X PUT $cds_baseurl/catalog/$cid \
        -H "accept: */*" -H "Content-Type: application/json" \
        -d @$jsonin
      if [[ "$(jq '.status' $jsonout)" != "200" ]]; then
        cat $jsonin
        cat $jsonout
        fail "Catalog update failed"
      fi
      rm $jsonin
      rm $jsonout
    fi
    cat=$((cat+1))
  done
}

function setup_federation() {
  trap 'fail' ERR
  log "Checking for 'self' peer entry for $GRAPHENE_ORIGIN"
  local cds_baseurl="-k https://$GRAPHENE_DOMAIN/ccds"
  check_name_resolves cds-service
  if [[ $NAME_RESOLVES == "true" ]]; then
    cds_baseurl="http://cds-service:8000/ccds"
  fi
  local creds="$GRAPHENE_CDS_USER:$GRAPHENE_CDS_PASSWORD"
  local t=0
  local jsonout="/tmp/$(uuidgen)"
  curl -s -o $jsonout -u $creds $cds_baseurl/peer
  while [[ $(grep -c numberOfElements $jsonout) -eq 0 ]]; do
    if [[ $t -eq $GRAPHENE_SUCCESS_WAIT_TIME ]]; then
      fail "CDS API is not ready after $GRAPHENE_SUCCESS_WAIT_TIME seconds"
    fi
    log "CDS API is not yet ready; waiting 10 seconds"
    t=$((t+10))
    sleep 10
    curl -s -o $jsonout -u $creds $cds_baseurl/peer
  done
  cat $jsonout
  local peers=$(jq '.content | length' $jsonout)
  local peer=0
  local found=no
  while [[ $peer -lt $peers ]]; do
    if [[ "$(jq -r ".content[$peer].name" $jsonout)" == "$GRAPHENE_FEDERATION_DOMAIN" ]]; then
      found=yes
    fi
    peer=$((peer+1))
  done
  if [[ "$found" == "no" ]]; then
    log "Create 'self' peer entry (required) via CDS API"
    local jsonin="/tmp/$(uuidgen)"
    cat <<EOF >$jsonin
{
"name": "$GRAPHENE_CERT_SUBJECT_NAME",
"self": true,
"local": false,
"contact1": "$GRAPHENE_ADMIN_EMAIL",
"subjectName": "$GRAPHENE_CERT_SUBJECT_NAME",
"apiUrl": "https://$GRAPHENE_FEDERATION_DOMAIN:$GRAPHENE_FEDERATION_PORT",
"statusCode": "AC",
"validationStatusCode": "PS"
}
EOF
    curl -s -o $jsonout -u $creds -X POST $cds_baseurl/peer \
      -H "accept: */*" -H "Content-Type: application/json" \
      -d @$jsonin
    if [[ "$(jq -r '.created' $jsonout)" == "null" ]]; then
      cat $jsonout
      rm $jsonout $jsonin
      fail "Peer entry creation failed"
    fi
    rm $jsonin
  else
    log "Self peer entry already exists for $GRAPHENE_DOMAIN"
  fi
  rm $jsonout

  log "Update default catalog attributes"
  # Needed to avoid subscription issues due to conflicting catalog attributes,
  # i.e. peers cannot have the exact same catalog name
  # Note: the name must be less than 50 chars
  name=$(echo $GRAPHENE_DOMAIN | cut -d '.' -f 1)
  customize_catalog 'Public Models' PB "$name Public"
  customize_catalog 'Company Models' RS "$name Internal"
}

set -x
trap 'fail' ERR
WORK_DIR=$(pwd)
cd $(dirname "$0")
source utils.sh
update_graphene_env AIO_ROOT $WORK_DIR force
source graphene_env.sh
update_graphene_env DEPLOY_RESULT "" force
update_graphene_env FAIL_REASON "" force
cat <<EOF >status.sh
EOF
set_k8s_env

get_host_ip $GRAPHENE_DOMAIN
update_graphene_env GRAPHENE_DOMAIN_IP $HOST_IP force

get_host_ip $GRAPHENE_HOST
update_graphene_env GRAPHENE_HOST_IP $HOST_IP force
update_graphene_env GRAPHENE_JWT_KEY $(uuidgen)
update_graphene_env GRAPHENE_CDS_PASSWORD $(uuidgen)

log "Apply environment customizations to unset values in graphene_env.sh"
source graphene_env.sh

prepare_env
bash $AIO_ROOT/setup_keystore.sh

# Ingress controller setup needs to precede ingress creations
setup_ingress_controller

# Graphene components depend upon pre-configuration of Nexus (e.g. ports)
if [[ "$GRAPHENE_DEPLOY_NEXUS" == "true" && "$GRAPHENE_CDS_PREVIOUS_VERSION" == "" ]]; then
  bash $AIO_ROOT/nexus/setup_nexus.sh clean
  bash $AIO_ROOT/nexus/setup_nexus.sh setup
  # Prevent redeploy from reinstalling Nexus unless specifically requested
  update_graphene_env GRAPHENE_DEPLOY_NEXUS false force
fi
# Apply any env updates from above
source graphene_env.sh
add_to_urls Nexus http://$GRAPHENE_NEXUS_DOMAIN:$GRAPHENE_NEXUS_API_PORT

if [[ "$GRAPHENE_DEPLOY_NEXUS_REPOS" == "true" && "$GRAPHENE_CDS_PREVIOUS_VERSION" == "" ]]; then
  bash $AIO_ROOT/nexus/setup_nexus_repos.sh all
  # Prevent redeploy from reinstalling Nexus unless specifically requested
  update_graphene_env GRAPHENE_DEPLOY_NEXUS_REPOS false force
fi

# ELK and Graphene core components depend upon pre-configuration of MariaDB
if [[ "$GRAPHENE_DEPLOY_MARIADB" == "true" && "$GRAPHENE_CDS_PREVIOUS_VERSION" == "" ]]; then
  source $AIO_ROOT/../charts/mariadb/setup_mariadb_env.sh
  bash $AIO_ROOT/mariadb/setup_mariadb.sh
  # Prevent redeploy from reinstalling MariaDB unless specifically requested
  update_graphene_env GRAPHENE_DEPLOY_MARIADB false force
fi
# Apply any env updates from above
source graphene_env.sh

# Supports use cases: MariaDB pre-setup (GRAPHENE_DEPLOY_MARIADB=false),
# MariaDB new install, and database upgrade
if [[ "$GRAPHENE_CDS_VERSION" != "$GRAPHENE_CDS_PREVIOUS_VERSION" ]]; then
  update_graphene_env GRAPHENE_SETUP_DB true
fi
if [[ "$GRAPHENE_SETUP_DB" == "true" ]]; then
  bash $AIO_ROOT/setup_graphenedb.sh
  # Prevent redeploy from resetting database unless specifically requested
  update_graphene_env GRAPHENE_CDS_PREVIOUS_VERSION $GRAPHENE_CDS_VERSION force
fi

# Apply any env updates from above
source graphene_env.sh

if [[ "$DEPLOYED_UNDER" == "k8s" ]]; then
  if [[ "$GRAPHENE_DEPLOY_INGRESS_RULES" == "true" ]]; then
    if [[ "$GRAPHENE_INGRESS_SERVICE" == "nginx" ]]; then
      bash $AIO_ROOT/ingress/setup_ingress.sh
    fi
  fi
fi

if [[ "$GRAPHENE_DEPLOY_CORE" == "true" ]]; then
  setup_graphene
  update_graphene_env GRAPHENE_DEPLOY_CORE false force
fi

if [[ "$GRAPHENE_DEPLOY_FEDERATION" == "true" ]]; then
  setup_federation
  update_graphene_env GRAPHENE_DEPLOY_FEDERATION false force
fi

if [[ "$GRAPHENE_DEPLOY_LUM" == "true" ]]; then
  bash $AIO_ROOT/lum/setup-lum.sh
  update_graphene_env GRAPHENE_DEPLOY_LUM false force
  add_to_urls "License Usage Manager" http://$GRAPHENE_DOMAIN/lum/
fi

log "Setup SV site-config"
check_name_resolves sv-scanning-service
if [[ $NAME_RESOLVES == "true" ]]; then
  sv_baseurl="http://sv-scanning-service:9082/"
else
  sv_baseurl="-k https://$GRAPHENE_ORIGIN/sv"
fi
curl $sv_baseurl/update/siteConfig/verification

set +x

cd $AIO_ROOT
cat <<EOF >status.sh
DEPLOY_RESULT=success
FAIL_REASON=
EOF
update_graphene_env DEPLOY_RESULT success force

log "Deploy is complete."
echo "You can access the Graphene portal and other services at the URLs below,"
echo "assuming hostname \"$GRAPHENE_DOMAIN\" is resolvable from your workstation:"

add_to_urls "Common Data Service Swagger UI" https://$GRAPHENE_ORIGIN/ccds/swagger-ui.html
add_to_urls "Portal Swagger UI" https://$GRAPHENE_ORIGIN/api/swagger-ui.html
add_to_urls "Onboarding Service Swagger UI" https://$GRAPHENE_ORIGIN/onboarding-app/swagger-ui.html
if [[ "$GRAPHENE_ELK_DOMAIN" != "" ]]; then
  add_to_urls Kibana http://$GRAPHENE_ELK_DOMAIN:$GRAPHENE_ELK_KIBANA_PORT/app/kibana
fi

if [[ "$DEPLOYED_UNDER" == "docker" ]]; then
  add_to_urls "Mariadb Admin" http://$GRAPHENE_HOST_IP:$GRAPHENE_MARIADB_ADMINER_PORT
fi
cat graphene.url
