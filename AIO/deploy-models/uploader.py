#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2023 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import json
import os
import re
from typing import Dict

import numpy as np
import requests

from document_manager import DocumentManager
from model import Model, Pipeline, Solution
from utils import APIClient
import uuid
import time

def load_models_from_json(json_file_name: str='tutorial_solutions.json'):
    """
    Reads the details of all models to be onboarded and published from json file, and loads them into the memory.
    :param json_file_name: location of the json file.
    :return: dictionary with details of each model to be uploaded to the graphene host.
    """
    models_dict = {}
    pipeline_dicts = {}
    tutorial_pipelines_metadata = json.load(open(json_file_name, 'r'))
    solutionId_modelName_map = {}
    nodeId_modelName_map = {}
    json_file_loc = os.path.dirname(json_file_name)
    for tutorial_pipeline_metadata in tutorial_pipelines_metadata:
        pipeline_loc = tutorial_pipeline_metadata['location']
        pipeline_name = tutorial_pipeline_metadata['pipeline_name']
        pipeline_solution_path = os.path.join(json_file_loc, pipeline_loc, 'solution.json')
        pipeline_solution_details = json.load(open(pipeline_solution_path))
        pipeline = Pipeline(pipeline_solution_details['pipeline_name'], pipeline_solution_details, pipeline_loc)
        pipeline.models = {model_name: '' for model_name in pipeline_solution_details['models']}
        for model_name in pipeline.models.keys():
            if model_name in models_dict:
                pipeline.models[model_name] = models_dict[model_name]['model']
                continue
            model_path = os.path.join(pipeline_loc, model_name)
            model_solution_path = os.path.join(model_path, 'solution.json')
            model_solution = json.load(open(model_solution_path))
            model_obj = Model(model_name, model_solution, model_path)
            models_dict[model_name] = {'model': model_obj, 'pipeline': pipeline_name}
            pipeline.models[model_name] = model_obj
        pipeline_dicts[pipeline_name] = pipeline
    return models_dict, pipeline_dicts, solutionId_modelName_map


class SolutionUploader(object):
    """
    This class reads the model's configurations from a json file and onboards & publishes it to the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    - GRAPHENE_DOC_USERID
    """

    def __init__(
            self,
            solutionId_modelName_map
    ) -> None:
        # setup parameters from reading environment variables.
        self.GRAPHENE_HOST = os.environ['GRAPHENE_HOST']  # FQHN like aiexp-preprod.ai4europe.eu
        self.REGISTRY_HOST = os.environ['REGISTRY_HOST']
        self.GRAPHENE_TOKEN = os.environ['GRAPHENE_TOKEN']  # format is 'graphene_username:API_TOKEN'
        self.GRAPHENE_USER = os.environ['GRAPHENE_USER']
        self.GRAPHENE_PW = os.environ['GRAPHENE_PW']
        self.GRAPHENE_CATALOGID = os.environ['GRAPHENE_CATALOGID']
        self.GRAPHENE_USERID = os.environ['GRAPHENE_USERID']
        self.GRAPHENE_DOC_USERID = os.environ['GRAPHENE_DOC_USERID']
        self.solutionId_modelName_map = solutionId_modelName_map
        self.docker_base = f'{self.REGISTRY_HOST}:7444/ai4eu-experiments/openml'
        self.document_mgr = DocumentManager()
        self.header_type = {
            "Content-Type": "application/json",
            "Authorization": self.GRAPHENE_TOKEN,
        }
        self.host = f'https://{self.GRAPHENE_HOST}'
        self.onboarding_host = f'https://{self.GRAPHENE_HOST}:443'
        self.api_client = APIClient(self.GRAPHENE_USER, self.GRAPHENE_PW, self.host)
        self.onboarding_api_client = APIClient(self.GRAPHENE_USER, self.GRAPHENE_PW, self.onboarding_host)

    def set_authors_publisher(self, solution: Solution, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to set the author and publisher details of the onboarded solution.
        This is one of the pre-publication step.
        :param solution: holds required details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the author and publisher details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        author_api = f'ccds/solution/{solution_id}/revision/{revision_id}'
        status = self.api_client.get_put_request(author_api, data=solution.authors, headers=self.header_type)
        print(f'Set Authors Status: {status}')
        return status

    def set_description(self, solution: Solution, solution_id: str, revision_id: str, catalog_id: str) -> bool:
        """
        This method calls an api to set the solution description details of the onboarded solution.
        This is one of the pre-publication step.
        :param solution: holds required details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the description details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        desc_api = f'ccds/revision/{revision_id}/catalog/{catalog_id}/descr'
        data = {
            "description": solution.description
        }
        set_descrp_status = self.api_client.post_request(desc_api, data=json.dumps(data), headers=self.header_type)
        print(f'Setup Description Status: {set_descrp_status}')
        return set_descrp_status

    def set_tags(self, solution: Solution, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to set the tags of the onboarded solution.
        This is one of the pre-publication step.
        :param solution: holds required details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the tags.
        :return: boolean value, where `True` indicates the call was successful.
        """
        tag_api = f'ccds/solution/{solution_id}'
        data = solution.tags
        response = self.api_client.get_put_request(tag_api, data=data, headers=self.header_type)
        if response:
            tag_api = f'ccds/catalog/{self.GRAPHENE_CATALOGID}/solution/{solution_id}'
            status = self.api_client.post_request(tag_api, data=json.dumps(data), headers=self.header_type)
            print(f'Set Tags Status: {status}')
            return True
        print(f'Set Tags Status: {False}')
        return False

    def host_documents_nexus(self, solution: Solution) -> bool:
        """
        This method uploads all the documents associated with a solution to nexus maven repository.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, where `True` means upload was successful, `False` otherwise
        """
        default_version = '1.0.0'
        for doc_name in solution.doc_list:
            doc_location = os.path.join(solution.local_fpath, doc_name)
            solution_id = solution.solution_id
            doc_link = self.document_mgr.upload_document(solution_id, doc_name, doc_location, default_version)
            solution.documents[doc_name] = {
                'doc_link': doc_link,
                'doc_location': doc_location,
                'doc_size': os.path.getsize(doc_location)}
        return True

    def create_documents_graphene(self, solution: Solution) -> bool:
        """
        This method stores links to the documents stored on nexus, which are associated with a solution.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        api_url = 'ccds/document'
        for doc_name in solution.doc_list:
            doc_details = solution.documents[doc_name]
            data = {
                "name": doc_name,
                "uri": doc_details['doc_link'],
                "size": doc_details['doc_size'],
                "userId": self.GRAPHENE_DOC_USERID
            }
            response = self.api_client.post_request(api_url, data=json.dumps(data))
            solution.documents[doc_name]['document_id'] = response.json()['documentId']
            return True
        return True

    def link_document_to_solution(self,
                                  catalog_id: str,
                                  solution: Solution
                                  ) -> bool:
        """
        This method links the documents stored on graphene, with a revision id of a solution in a catalog.
        It is one of the pre-publication step.
        :param catalog_id: identifier of the catalog, to which the solution belongs to.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        revision_id = solution.revision_id
        for doc_name in solution.doc_list:
            document_id = solution.documents[doc_name]['document_id']
            api_url = f"ccds/revision/{revision_id}/catalog/{catalog_id}/document/{document_id}"
            self.api_client.post_request(api_url, None)
        return True

    def upload_documents(self, solution: Solution) -> bool:
        """
        This method has three steps:
        1. Stores document on the nexus repository.
        2. Links the documents stored on nexus, with document controller of graphene.
        3. Links the documents stored on graphene, with a revision id of a solution in a catalog.
        It is one of the pre-publication step.
        :param solution: solution object, for which we need to upload the documents.
        :returns: bool value, `True` meaning task was success, otherwise `False`
        """
        hosting_status = self.host_documents_nexus(solution)
        creating_status = self.create_documents_graphene(solution)
        linking_status = self.link_document_to_solution(self.GRAPHENE_CATALOGID, solution)
        print(f'Upload Document Status: {True}')
        return hosting_status and creating_status and linking_status

    def set_icon(self, solution: Solution, solution_id: str) -> bool:
        """
        This method calls an api to set the icon of the onboarded solution.
        This is one of the pre-publication step.
        :param solution: holds required details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: boolean value, where `True` indicates the call was successful.
        """
        icon_api = f'ccds/solution/{solution_id}/pic'
        status = self.api_client.put_image_request(icon_api, data=solution.icon, headers=None)
        print(f'Set Icon Status: {status}')
        return status

    def get_latest_version_number_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: the latest revision number for the onboarded solution.
        """
        revision_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}/revision'
        response = requests.get(revision_api, auth=(self.GRAPHENE_USER, self.GRAPHENE_PW))
        if response.status_code == 200:
            body = json.loads(response.text)
            if body:
                version = np.array([i['version'] for i in body]).argmax()
                return (body[version]['version'], body[version]['revisionId'])
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0
    
    def get_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: the latest revision number for the onboarded solution.
        """
        revision_api = f'https://{self.GRAPHENE_HOST}/ccds/solution/{solution_id}/revision'
        response = requests.get(revision_api, auth=(self.GRAPHENE_USER, self.GRAPHENE_PW))
        if response.status_code == 200:
            body = json.loads(response.text)
            if body:
                version = np.array([i['version'] for i in body]).argmax()
                return body[version]['revisionId']
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0

    def pre_publication_steps(self, solution: Solution, solution_id: str, revision_id: str) -> bool:
        """
        This method calls the four pre-publication steps to set the solution's:
        - Authors' and publishers' details.
        - Tags.
        - Description.
        - Icon file.
        - Documents.
        :param solution: holds required details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the required details.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        auto_status = self.set_authors_publisher(solution, solution.solution_id, solution.revision_id)
        tags_status = self.set_tags(solution, solution.solution_id, solution.revision_id)
        desc_status = self.set_description(solution, solution.solution_id, solution.revision_id,
                                           self.GRAPHENE_CATALOGID)
        icon_status = self.set_icon(solution, solution.solution_id)
        docs_status = self.upload_documents(solution)

        return auto_status and tags_status and desc_status and icon_status and docs_status
    
    def publish_model_approval_request(self, model: Model) -> object:
        """
        This method onboards and publishes the model.
        :param model: holds required details of the onboarded model.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        onboarding_resp = self.onboard_model(model=sample_model)
        if onboarding_resp:
            solution_id = onboarding_resp['solution_id']
            task_id = onboarding_resp['task_id']
            tracking_id = onboarding_resp['tracking_id']
            user_id = onboarding_resp['user_id']
            revision_id = self.get_revision_id(solution_id)
            model.revision_id = revision_id
            pre_publish_result = self.pre_publication_steps(model, solution_id, revision_id)
            if pre_publish_result:
                publish_url = f"ccds/pubreq"
                payload = {
                    "catalogId": self.GRAPHENE_CATALOGID,
                    "reviewUserId": self.GRAPHENE_USERID,
                    "requestUserId": self.GRAPHENE_USERID,
                    "revisionId": revision_id,
                    "solutionId": solution_id,
                    "statusCode": "AP"
                }
                pub_resp = self.api_client.post_request(publish_url, data=json.dumps(payload), headers=self.header_type)
                return pub_resp
        else:
            print('One of the pre-publication steps failed, please try again with correct configurations')
            return False
        
if __name__ == '__main__':
    from model_uploader import ModelUploader
    from pipeline_uploader import PipelineUploader
    models, pipelines, solutionId_modelName_map = load_models_from_json(json_file_name='tutorial_solutions.json')
    # Above line works on the server
    # models, pipelines, solutionId_modelName_map = load_models_from_json(json_file_name='AIO/deploy-models/tutorial_solutions.json')
    pipeline_uploader = PipelineUploader(solutionId_modelName_map)
    model_responses = {}
    # Onboarding all the models

    for pipeline_key in pipelines.keys():
        time.sleep(2)
        cid = pipeline_uploader.create_new_composite_solution()
        if not cid:
            break
        current_pipeline = pipelines[pipeline_key]
        model_responses.update(pipeline_uploader.upload_all_models(current_pipeline))
        current_pipeline.cid = cid
        for model_key in current_pipeline.models.keys():
            model_node = current_pipeline.models[model_key]
            pipeline_uploader.add_pipeline_node(model_node, cid)
        node_links=current_pipeline.solution['add_link_params']
        for node_link_params in node_links:
            add_link_resp = pipeline_uploader.add_pipeline_link(current_pipeline, node_link_params)
        modify_nodes=current_pipeline.solution['modify_node_urls']
        for modify_node_api in modify_nodes:
            modify_node_resp = pipeline_uploader.modify_node(current_pipeline, modify_node_api)
        response=pipeline_uploader.save_composite_solution(current_pipeline)
        solution_id = response.json()['solutionId']
        revision_id = response.json()['revisionId']
        version = int(response.json()['version'])
        current_pipeline.solution_id=solution_id
        current_pipeline.revision_id = revision_id
        is_val_solution = pipeline_uploader.validate_solution(current_pipeline)
        if is_val_solution:
            blueprint_resp,cdump_resp=pipeline_uploader.get_cdump_blueprint(current_pipeline)
            blueprint_resp
            pre_publ_resp = pipeline_uploader.pre_publication_steps(current_pipeline, solution_id, revision_id)
            pre_publ_resp