#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2023 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import os
import shutil
from io import BytesIO
from typing import Dict, List

import numpy as np
from PIL import Image

from model import Pipeline
from utils import APIClient


class SolutionRetriever(object):
    """
    This class reads the list of solution ids belonging to either models or pipelines.
    And downloads all artifacts related to a solution.
    This class can be configured from a json file, and it downloads solutions from the specified host.
    It reads a lot of authentication related details from environment variables, make sure to set relevant details.
    The required environment variables are:
    - GRAPHENE_HOST
    - REGISTRY_HOST
    - GRAPHENE_TOKEN
    - GRAPHENE_USER
    - GRAPHENE_PW
    - GRAPHENE_CATALOGID
    - GRAPHENE_USERID
    """

    def __init__(
            self
    ) -> None:
        # setup parameters from reading environment variables.
        self.GRAPHENE_HOST = os.environ['GRAPHENE_HOST']  # FQHN like aiexp-preprod.ai4europe.eu
        self.REGISTRY_HOST = os.environ['REGISTRY_HOST']
        self.GRAPHENE_TOKEN = os.environ['GRAPHENE_TOKEN']  # format is 'graphene_username:API_TOKEN'
        self.GRAPHENE_USER = os.environ['GRAPHENE_USER']
        self.GRAPHENE_PW = os.environ['GRAPHENE_PW']
        self.GRAPHENE_CATALOGID = os.environ['GRAPHENE_CATALOGID']
        self.GRAPHENE_USERID = os.environ['GRAPHENE_USERID']
        self.docker_base = f'{self.REGISTRY_HOST}:7444/ai4eu-experiments/openml'
        self.ret_dir_name = "retrieved"
        self.header_type = {
            "Content-Type": "application/json",
            "Authorization": self.GRAPHENE_TOKEN,
        }
        self.host = f'https://{self.GRAPHENE_HOST}'
        self.api_client = APIClient(self.GRAPHENE_USER, self.GRAPHENE_PW, self.host)

    def get_solution(self, solution_id: str) -> Dict:
        """
        Retrieves the solution (model/pipeline) from the graphene host.
        :param solution_id: solution identifier to retrieve appropriate model/pipeline from the graphene host.
        :return: dictionary object with relevant details of the retrieved solution.
        """
        resp = self.api_client.get_request(f"ccds/solution/{solution_id}")
        return resp

    def get_revision_id(self, solution_id: str):
        """
        This method calls an api to get the latest revision id of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :return: the latest revision number for the onboarded solution.
        """
        revision_api = f'ccds/solution/{solution_id}/revision'
        response = self.api_client.get_request(revision_api)
        if response:
            version = np.array([i['version'] for i in response]).argmax()
            return response[version]['revisionId']
        else:
            print(f'Can not get Revision ID. Error code: {response.status_code}')
            return 0

    def get_authors_publisher(self, solution_id: str, revision_id: str) -> bool:
        """
        This method calls an api to get the author and publisher details of the onboarded solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the author and publisher details.
        :return: boolean value, where `True` indicates the call was successful.
        """
        author_api = f'ccds/solution/{solution_id}/revision/{revision_id}'
        response = self.api_client.get_request(author_api)
        authors = None
        publisher = None

        if "authors" in response:
            authors = response["authors"]
        if "publisher" in response:
            publisher = response["publisher"]
        print(f'Authors and Publisher: \n{authors}\n{publisher}')
        return {'authors': authors, "publisher": publisher}

    def get_description(self, revision_id: str, catalog_id: str) -> Dict:
        """
        This method calls an api to get the solution description of the solution.
        :param revision_id: current revision number for which we need to get the description.
        :param catalog_id: catalog identifier in which the solution is available.
        :return: string that represents the solution's description.
        """
        desc_api = f'ccds/revision/{revision_id}/catalog/{catalog_id}/descr'
        response = self.api_client.get_request(desc_api)
        description = None
        if "description" in response:
            description = response["description"]
        print(f'Setup Description Status: {description}')
        return {"description": description}

    def get_tags(self, solution_id: str, revision_id: str) -> List[Dict]:
        """
        This method calls an api to get the tags of the solution.
        :param solution_id: solution identifier for the onboarded solution.
        :param revision_id: current revision number for which we need to set the tags.
        :return: list of tags.
        """
        tag_api = f'ccds/solution/{solution_id}'
        response = self.api_client.get_request(tag_api)
        if 'tags' in response:
            tags = response['tags']
            return {"tags": tags}
        return {"tags": None}

    def get_icon(self, solution_id: str) -> bool:
        """
        This method calls an api to get the icon of the solution.
        :param solution_id: solution identifier for the solution.
        :return: bytes string that represent the solution's icon.
        """
        icon_api = f'ccds/solution/{solution_id}/pic'
        icon = self.api_client.get_image(icon_api)
        return icon

    def store_icon(self, icon: bytes, folder_location: str):
        """
        Store the icon available in bytes format to the provided directory.
        :param icon: the bytes string representation of the solution's icon. The icon will be stored either as `icon.png` or `icon.jpg` depending upon the file retrieved from server.
        :param folder_location: location of the solution folder
        Reference: https://stackoverflow.com/a/23489503
        """
        file_name = None
        if "PNG" in str(icon):
            file_name = "icon.png"
        else:
            file_name = "icon.jpg"
        icon_img = Image.open(BytesIO(icon))
        icon_img.save(os.path.join(folder_location, file_name))

    def create_retrieval_dir(self, delete_old_files=False):
        """
        Creates a directory to store all the retrieved solutions.
        :param delete_old_files: a flag, when set True, deletes all previous solutions in the retreival directory.
        """
        if os.path.exists(self.ret_dir_name):
            if delete_old_files:
                shutil.rmtree(self.ret_dir_name)
            else:
                print("Not deleting old content")
        os.makedirs(self.ret_dir_name, exist_ok=True)

    def download_solution(self, solution_id: str) -> bool:
        """
        Downloads the solution and stores all its corresponding files in a folder with name of the solution.
        :param solution_id: solution identifier to download details of the solution.
        :return: boolean value, where True means download was success, False meaning failure occured.
        """
        self.create_retrieval_dir(False)
        solution = self.get_solution(solution_id)
        revision_id = self.get_revision_id(solution_id)
        icon = self.get_icon(solution_id)
        tags = self.get_tags(solution_id, revision_id)
        description = self.get_description(revision_id, soln_retriever.GRAPHENE_CATALOGID)
        authors = self.get_authors_publisher(solution_id, revision_id)
        solution = solution | tags | description | authors
        is_model = False
        soln_object = None
        if 'toolkitTypeCode' in solution:
            soln_name = solution['name']
            if solution['toolkitTypeCode'] == 'CP':
                soln_path = os.path.join(self.ret_dir_name, soln_name)
                soln_object = Pipeline(soln_name, solution, soln_path, True)
                soln_object
            else:
                print("Model")
                soln_path = os.path.join(self.ret_dir_name, soln_name)
                soln_object = Pipeline(soln_name, solution, soln_path, True)


if __name__ == "__main__":
    # solution_id = "8b965f7a-2787-41cf-9980-0a2bcefb0d01"
    # solution_id = "f88d26a5-53a1-45b4-a587-5f0481da1884"
    # solution_id = "e33a3ded-b857-4f2c-8055-5c73662a678b"
    # solution_id = '8932a236-a024-4eda-ac5a-bf68b59543ad'
    solution_id = "41218ad2-efe7-4822-b632-d52dcc2ad150"
    soln_retriever = SolutionRetriever()
    soln = soln_retriever.download_solution(solution_id)
    print("")
