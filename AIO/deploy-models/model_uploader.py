#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2023 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import json
import os
import re
from typing import Dict

import numpy as np
import requests

from document_manager import DocumentManager
from model import Model, Pipeline, Solution
from utils import APIClient
import uuid
from uploader import SolutionUploader

class ModelUploader(SolutionUploader):
    def onboard_model(self, model: Model):
        """
        Onboards the model at the graphene host.
        :param model: Model object holds the details of the model to be onboarded.
        :return: dictionary object with relevant details of onboarded model such as `solution_id`.
        """
        # Setup parameters & call API to onboard model
        advanced_api = f"onboarding-app/v2/advancedModel"
        files = model.onboarding_files
        headers = {
            "Accept": "application/json",
            "modelname": model.name,
            "Authorization": self.GRAPHENE_TOKEN,
            "dockerFileURL": model.docker_uri,
            'isCreateMicroservice': 'false',
            'description': 'Description from the header!'
        }

        # send request
        response = self.onboarding_api_client.post_document_request_wo_auth(advanced_api, params=None, files=files,
                                                                            headers=headers)

        # check response
        if response.status_code == 201:
            # Retrieve relevant details from the response
            body = json.loads(response.text)
            solution_id: str = body['result']['solutionId']
            onboarding_resp: Dict = {
                'solution_id': solution_id,
                'task_id': body['taskId'],
                'tracking_id': body['trackingId'],
                'user_id': body['result']['userId']
            }
            model.solution_id = solution_id
            print(
                f"Docker uri is pushed successfully on {self.GRAPHENE_HOST},  response is: {response.status_code}, solution id: {solution_id}\n")
            return onboarding_resp

        print(f"Docker uri is not pushed on {self.GRAPHENE_HOST}, response is: {response.status_code}, \n")
        return False

    def publish_model_approval_request(self, model: Model) -> object:
        """
        This method onboards and publishes the model.
        :param model: holds required details of the onboarded model.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        onboarding_resp = self.onboard_model(model=model)
        if onboarding_resp:
            solution_id = onboarding_resp['solution_id']
            task_id = onboarding_resp['task_id']
            tracking_id = onboarding_resp['tracking_id']
            user_id = onboarding_resp['user_id']
            revision_id = self.get_revision_id(solution_id)
            model.revision_id = revision_id
            pre_publish_result = self.pre_publication_steps(model, solution_id, revision_id)
            if pre_publish_result:
                publish_url = f"ccds/pubreq"
                payload = {
                    "catalogId": self.GRAPHENE_CATALOGID,
                    "reviewUserId": self.GRAPHENE_USERID,
                    "requestUserId": self.GRAPHENE_USERID,
                    "revisionId": revision_id,
                    "solutionId": solution_id,
                    "statusCode": "AP"
                }
                pub_resp = self.api_client.post_request(publish_url, data=json.dumps(payload), headers=self.header_type)
                return pub_resp
        else:
            print('One of the pre-publication steps failed, please try again with correct configurations')
            return False