#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2023 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================

import json
import os
import re
from typing import Dict

import numpy as np
import requests

from document_manager import DocumentManager
from model import Model, Pipeline, Solution
from utils import APIClient
import uuid
from uploader import SolutionUploader
from model_uploader import ModelUploader

class PipelineUploader(SolutionUploader):
    
    def __init__(
            self,
            solutionId_modelName_map
    ) -> None:
        SolutionUploader.__init__(self, solutionId_modelName_map)
        self.model_uploader = ModelUploader(solutionId_modelName_map)
    def onboard_pipeline(self, pipeline: Pipeline):
        """
        Onboards the pipeline at the graphene host.
        :param pipeline: Pipeline object holds the details of the pipeline to be onboarded.
        :return: dictionary object with relevant details of onboarded model such as `solution_id`.
        """
        # Setup parameters & call API to onboard model
        advanced_api = f"onboarding-app/v2/onboardPipeline"
        files = pipeline.onboarding_files
        headers = {
            "Accept": "application/json",
            "modelname": pipeline.name,
            "Authorization": self.GRAPHENE_TOKEN
        }

        # send request
        response = self.onboarding_api_client.post_document_request_wo_auth(advanced_api, params=None, files=files,
                                                                            headers=headers)
        pipeline.cleanup_sample_onboarding_files()
        # check response
        if response.status_code == 201:
            # Retrieve relevant details from the response
            body = json.loads(response.text)
            solution_id: str = body['result']['solutionId']
            onboarding_resp: Dict = {
                'solution_id': solution_id,
                'task_id': body['taskId'],
                'tracking_id': body['trackingId'],
                'user_id': body['result']['userId']
            }
            pipeline.solution_id = solution_id
            print(
                f"Docker uri is pushed successfully on {self.GRAPHENE_HOST},  response is: {response.status_code}, solution id: {solution_id}\n")
            return onboarding_resp

        print(f"Docker uri is not pushed on {self.GRAPHENE_HOST}, response is: {response.status_code}, \n")
        return False
    
    def validate_solution(self, solution: Solution):
        """
        Check if the uploaded composite solution is validated by the design studio api.
        """
        validate_solution_api = f'dsce/dsce/solution/validateCompositeSolution'
        version_number, revision_id = self.get_latest_version_number_revision_id(solution.solution_id)
        params = {
                "userId": self.GRAPHENE_USERID,
                "solutionName": solution.name,
                "solutionId": solution.solution_id,
                "version": int(version_number.split(".")[0])
            }
        response = self.api_client.post_request(validate_solution_api, params=params)
        status_code = response.status_code
        result = False
        if status_code == 200:
            if str.capitalize(json.loads(response.text)['success']) == 'False':
                print(f"Model Validation Failed!")
            else:
                print(f"Model Validation Succeeded!")
                result = True
            print(f"Response received: {response.text}")
        return result
    
    def create_new_composite_solution(self):
        """
        Creates a new composite solution, which is equivalent to initializing a blank canvas on design studio.
        """
        create_comp_sol_api = "dsce/dsce/solution/createNewCompositeSolution"
        params = {
            "userId":self.GRAPHENE_USERID
        }
        response = self.api_client.post_request(create_comp_sol_api, params=params)
        status_code = response.status_code
        if status_code == 200:
            return json.loads(response.text)['cid']
        return False
    
    def add_pipeline_node(self, model_node: Model, cid: str):
        """
        This method adds a models as a node to the pipeline(composite solution).
        :param model_node: holds required details of the model to be added as a node.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        add_node_api = "dsce/dsce/solution/addNode"  # POST
        params = {
            "userId":self.GRAPHENE_USERID,
            "cid": cid
        }
        add_node_payload_str = json.dumps(model_node.add_node_payload)
        solution_id = model_node.solution_id
        node_solution_id = model_node.nodeSolutionId
        nodeId = model_node.nodeId
        name = model_node.name
        add_node_payload_str = add_node_payload_str.replace(nodeId, name+"1")
        add_node_payload_str = add_node_payload_str.replace(node_solution_id, solution_id)
        response = self.api_client.post_request(add_node_api, data_json=json.loads(add_node_payload_str), params=params)
        if response.status_code == 200:
            if str.capitalize(json.loads(response.text)['success']) == 'False':
                print(f"Adding a new node to the pipeline failed!")
            else:
                print(f"Adding a new node to the pipeline succeeded!")
                result = True
            print(f"Response received: {response.text}")
        return result
    
    def add_pipeline_link(self, pipeline: Pipeline, params: Dict):
        """
        This method adds a link between models in the pipeline(composite solution).
        :param pipeline: holds required details of the pipeline to be added as a node.
        :param params: a dictionary that holds the json based payload for adding link between two nodes in the pipeline.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        add_link_api = pipeline.solution["add_link_url"]
        link_id = str(uuid.uuid1())
        pipeline.link_ids.append(link_id)
        params['userId']=self.GRAPHENE_USERID
        params['cid']=pipeline.cid
        params['linkId']=link_id
        for model_key in pipeline.models:
            model = pipeline.models[model_key]
            solution_id = model.solution_id
            node_solution_id = model.nodeSolutionId
            nodeId = model.nodeId
            name = model.name
            add_link_api = add_link_api.replace(node_solution_id, solution_id)
            add_link_api = add_link_api.replace(nodeId, name+"1")
            if model_key in params["sourceNodeName"]: 
                params["sourceNodeName"]=name+"1"
                params["sourceNodeId"]=name+"1"
            if model_key in params["targetNodeName"]:
                params["targetNodeName"]=name+"1"
                params["targetNodeId"]=name+"1"
        
        add_link_api = add_link_api.replace("{GRAPHENE_USER_ID}",self.GRAPHENE_USERID)
        add_link_api = add_link_api.replace("{COMP_SOLUTION_ID}",pipeline.cid)
        add_link_api = add_link_api.replace("{LINK_ID}",link_id)
        add_link_api="dsce/dsce/solution/addLink"        
        
        add_link_response = self.api_client.post_request(add_link_api, data_json={}, params=params)
        
        return add_link_response
    
    def modify_node(self, pipeline: Pipeline, modify_node_api: str):
        """
        This method modifies a node(model) in the pipeline(composite solution) on the design studio.
        :param pipeline: holds required details of the pipeline to be added as a node.
        :param modify_node_api: url to post the modification of the node.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        for model_key in pipeline.models:
            model = pipeline.models[model_key]
            solution_id = model.solution_id
            node_solution_id = model.nodeSolutionId
            nodeId = model.nodeId
            name = model.name
            modify_node_api = modify_node_api.replace(node_solution_id, solution_id)
            modify_node_api = modify_node_api.replace(nodeId, name+"1")
        modify_node_api = modify_node_api.replace("{GRAPHENE_USER_ID}",self.GRAPHENE_USERID)
        modify_node_api = modify_node_api.replace("{COMP_SOLUTION_ID}",pipeline.cid)
        modify_node_response = self.api_client.post_request(modify_node_api, None)
        return modify_node_response
    
    def save_composite_solution(self, pipeline: Pipeline):
        """
        This method adds a models as a node to the pipeline(composite solution).
        :param model_node: holds required details of the model to be added as a node.
        :return: boolean value, where `True` indicates all the calls were successful.
        """
        save_comp_sol_api="dsce/dsce/solution/saveCompositeSolution?userId={GRAPHENE_USER_ID}&solutionName={PIPELINE_NAME}&version=1&description=null&ignoreLesserVersionConflictFlag=false&cid={COMP_SOLUTION_ID}"
        save_comp_sol_api = save_comp_sol_api.replace("{GRAPHENE_USER_ID}",self.GRAPHENE_USERID)
        save_comp_sol_api = save_comp_sol_api.replace("{COMP_SOLUTION_ID}",pipeline.cid)
        save_comp_sol_api = save_comp_sol_api.replace("{PIPELINE_NAME}",pipeline.name)
        save_comp_sol_response = self.api_client.post_request(save_comp_sol_api)
        return save_comp_sol_response
    
    def get_cdump_blueprint(self, pipeline: Pipeline):
        """
        This method downloads cdump.json and blueprint.json of the published the pipeline (composite solution).
        :param pipeline: holds required details of the pipeline.
        :return: boolean value, where `True` indicates the model was published successfully.
        """
        get_rev_artifact_api = "ccds/revision/{REVISION_ID}/artifact"
        get_rev_artifact_api = get_rev_artifact_api.replace("{REVISION_ID}", pipeline.revision_id)
        comp_sol_resp = self.api_client.get_request(get_rev_artifact_api)
        blueprint_artifact_uri = None
        blueprint_artifact = None
        cdump_artifact_uri = None
        cdump_artifact = None
        for element in comp_sol_resp:
            if element['artifactTypeCode']=="BP":
                blueprint_artifact_uri=element["uri"]
                blueprint_artifact = self.document_mgr.get_document(blueprint_artifact_uri)
            if element['artifactTypeCode']=="CD":
                cdump_artifact_uri=element["uri"]
                cdump_artifact = self.document_mgr.get_document(cdump_artifact_uri)
        pipeline.cdump = json.loads(cdump_artifact['content'])
        pipeline.blueprint = json.loads(blueprint_artifact['content'])
        save_result = pipeline.save_onboarding_files()
        return (blueprint_artifact,cdump_artifact)
    
    def upload_all_models(self, pipeline: Pipeline):
        models = pipeline.models
        model_responses = {}
        for model_key in models.keys():
            current_model = models[model_key]
            model_name = current_model.name
            pub_resp = self.model_uploader.publish_model_approval_request(current_model)
            print(f'Model published: {model_name, pub_resp}')
            model_responses[model_key] = pub_resp
        return model_responses