#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import json
import os
from abc import ABC
from time import time
from typing import Dict

default_tags = {
    "modelTypeCode": "DS",
    "toolkitTypeCode": "SK",
    "tags": [
        {
            "tag": "OpenML"
        }
    ]
}


class Solution(ABC):
    """
    This class is the root class for all the models and pipelines in the graphene ecosystem.
    """

    def __init__(self,
                 solution_name: str = 'tutorial-solution',
                 solution_details: Dict = None,
                 local_fpath: str = '.',
                 is_retrieved: bool = False
                 ) -> None:
        # These fields are required during model onboarding step
        self.solution = solution_details
        self.solution_id = None
        self.revision_id = None
        self.TEST_MODE = 'TEST_MODE' in os.environ
        # Some additional fields that would be helpful
        self.local_fpath = local_fpath  # location of the model folder on current subsystem

        self.category = 'CS' if 'category' not in solution_details else solution_details['category']
        default_tags = [
            {"tag": "playground-ready"},
            {"tag": "tutorial"}
        ]

        self.tags = default_tags if 'tags' not in solution_details else solution_details['tags']
        self.doc_list = self.solution["documents"] if "documents" in self.solution else []
        self.user_id = os.environ['GRAPHENE_USERID']
        self.documents = {}
        # Some additional fields that would be helpful
        self.local_fpath = local_fpath  # location of the model folder on current subsystem

        default_author_name = "The Graphene Development Team"
        default_author_contact = "https://projects.eclipse.org/projects/technology.graphene"

        self.authors = None if 'authors' not in solution_details else solution_details['authors']
        if self.authors is None:
            self.authors = {
                "authors": [{
                    "name": default_author_name,
                    "contact": default_author_contact
                }],
                "publisher": default_author_name
            }
        license_filename = 'license.json' if 'license_file_name' not in solution_details else solution_details[
            'license_file_name']
        license_fp = os.path.join(self.local_fpath, license_filename)
        description_file_name = 'description.txt'
        description_fp = os.path.join(self.local_fpath, description_file_name)
        icon_filename = 'icon.jpg' if 'icon_filename' not in solution_details else solution_details['icon_filename']
        self.icon = None
        self.license = None
        self.description = None
        icon_fp = os.path.join(self.local_fpath, icon_filename)
        if is_retrieved or not self.TEST_MODE:
            self.base_solution_name = solution_name
            self.name = solution_name
        else:
            self.base_solution_name = solution_name
            self.name = self.generate_new_model_name()
            self.license = open(license_fp, 'rb')
            self.description = open(description_fp, 'r').read()  # provides the model description
            with open(icon_fp, 'rb') as icon_img:
                self.icon = icon_img.read()  # icon or thumbnail image for the model

    def generate_new_model_name(self) -> str:
        """
        Generates unix timestamp and appends it to the end of the model name.
        This makes sure everytime the model is onboarded, it has an unique name.
        """
        curr_time = int(time())
        new_model_name = f"{self.base_solution_name}-{str(curr_time)[-4:]}"
        return new_model_name


class Model(Solution):
    """
    This class holds all the information for a graphene model.
    """

    def __init__(self,
                 solution_name: str = 'tutorial-solution',
                 solution_details: Dict = None,
                 local_fpath: str = '.',
                 is_retrieved: bool = False
                 ) -> None:
        # These fields are required during model onboarding step
        super().__init__(solution_name, solution_details, local_fpath, is_retrieved)
        self.docker_uri = solution_details['docker_uri']
        self.proto_buf = 'model.proto' if 'protobuf' not in solution_details else solution_details['protobuf']
        # add_node_payload_fp = None
        self.add_node_payload=None
        
        # These fields are taken from the publication step
        self.documents_list = solution_details['documents']  # extra documents such as a README file or so on
        self.documents = {}
        user_id = os.environ['GRAPHENE_USERID']

        if len(self.name) == 0:
            # If the model name is empty
            self.name = os.path.basename(self.local_fpath)

        # Loading the associated files into the files dictionary
        proto_fp = os.path.join(self.local_fpath, self.proto_buf)
        add_node_payload_fp = os.path.join(self.local_fpath, "add_node_payload.json")
        self.proto_buf = open(proto_fp, 'rb')
        self.onboarding_files = {'license': ('license.json', self.license, 'application.json'),
                                 'protobuf': ('model.proto', self.proto_buf, 'text/plain')}
        self.nodeId = self.solution['nodeId']
        self.nodeSolutionId = self.solution['nodeSolutionId']
        self.add_node_payload=json.load(open(add_node_payload_fp, 'r'))


class Pipeline(Solution):
    """
    This class holds all the information for a graphene pipeline.
    """

    def __init__(self,
                 solution_name: str = 'tutorial-solution',
                 solution_details: Dict = None,
                 local_fpath: str = '.',
                 is_retrieved: bool = False
                 ) -> None:
        # These fields are required during model onboarding step
        super().__init__(solution_name, solution_details, local_fpath, is_retrieved)
        self.models = {}
        print(f'Model Name: {self.name}')
        self.license_fp = os.path.join(self.local_fpath, 'license.json')
        self.blueprint_fp = os.path.join(self.local_fpath, 'blueprint.json')
        self.cdump_fp = os.path.join(self.local_fpath, 'cdump.json')
        self.sample_cdump_fp = os.path.join(self.local_fpath, 'sample_cdump.json')
        self.sample_blueprint_fp = os.path.join(self.local_fpath, 'sample_blueprint.json')
        # These fields are taken from the publication step
        description_file_name = 'description.txt'
        description_fp = os.path.join(self.local_fpath, description_file_name)
        self.license = None
        self.blueprint = None
        self.cdump = None
        self.description = None
        self.add_link = None
        self.cid = None
        # add_link_fp = os.path.join(self.local_fpath,"add_link_request_link_post.txt")
        # modify_node_link_fp = os.path.join(self.local_fpath,"modify_node_post.txt")
        # save_comp_sol_link_fp = os.path.join(self.local_fpath,"save_composite_solution.txt")
        self.link_ids = []
        if not is_retrieved:
            self.license = open(self.license_fp, 'rb')
            # self.blueprint = open(self.blueprint_fp, 'rb')
            # self.cdump = open(self.cdump_fp, 'rb')
            self.description = open(description_fp, 'r').read()  # provides the model description
            # self.add_link=open(add_link_fp,'r').read()
            # self.modify_node_link = open(modify_node_link_fp,'r').read()
            # self.save_comp_sol_link = open(save_comp_sol_link_fp,'r').read()
        self.documents = {}
        if len(self.name) == 0:
            # If the model name is empty
            self.name = os.path.basename(self.local_fpath)

        # Loading the associated files into the files dictionary
        self.onboarding_files = {'license': ('license.json', self.license, 'application.json'),
                                 'blueprint': ('blueprint.json', self.blueprint, 'text/plain'),
                                 'cdump': ('cdump.json', self.cdump, 'text/plain')}

    def load_onboarding_files(self):
        """
        Prepares the three files required for onboarding a model - license.json, cdump.json, and blueprint.json.
        :return: None
        """
        self.license = open(self.license_fp, 'rb')
        self.blueprint = open(self.blueprint_fp, 'rb')
        self.cdump = open(self.cdump_fp, 'rb')

    def load_sample_onboarding_files(self, new_cdump: str, new_blueprint: str) -> bool:
        """
        Creates and loads the two new sample files - sample_cdump.json and sample_blueprint.json for onboarding a pipeline.
        :param new_cdump: str representation of new cdump file for onboarding a pipeline.
        :param new_blueprint: str representation of new blueprint file for onboarding a pipeline.
        :return: None
        """
        self.update_onboarding_files(new_cdump, new_blueprint)
        self.license = open(self.license_fp, 'rb')
        with open(self.sample_blueprint_fp, 'rb') as blueprint_file:
            self.blueprint = blueprint_file.read()
        with open(self.sample_cdump_fp, 'rb') as cdump_file:
            self.cdump = cdump_file.read()
        self.onboarding_files = {'license': ('license.json', self.license, 'application.json'),
                                 'blueprint': ('blueprint.json', self.blueprint, 'text/plain'),
                                 'cdump': ('cdump.json', self.cdump, 'text/plain')}

    # def update_onboarding_files(self, new_cdump: str, new_blueprint: str) -> bool:
    #     """
    #     Creates two new sample files - sample_cdump.json and sample_blueprint.json for onboarding a pipeline.
    #     :param new_cdump: str representation of new cdump file for onboarding a pipeline.
    #     :param new_blueprint: str representation of new blueprint file for onboarding a pipeline.
    #     :return: None
    #     """
    #     new_cdump = json.loads(new_cdump)
    #     new_blueprint = json.loads(new_blueprint)
    #     with open(self.sample_cdump_fp, 'w') as cdump_file:
    #         json.dump(new_cdump, cdump_file)
    #     with open(self.sample_blueprint_fp, 'w') as blueprint_file:
    #         json.dump(new_blueprint, blueprint_file)
    #     print("Files created!")
    
    def save_onboarding_files(self):
        with open(self.cdump_fp, 'w') as cdump_file:
            json.dump(self.cdump, cdump_file)
        with open(self.blueprint_fp, 'w') as blueprint_file:
            json.dump(self.blueprint, blueprint_file)
        self.load_onboarding_files()
        return True

    def cleanup_sample_onboarding_files(self):
        """
        Deletes the sample cdump.json and blueprint.json after pipeline has been successfully onboarded.
        :return: None
        """
        os.remove(self.sample_cdump_fp)
        os.remove(self.sample_blueprint_fp)
