#!/usr/bin/env python3
# =============================================================================
# Copyright (C) 2022 Fraunhofer Gesellschaft. All rights reserved.
# =============================================================================
# This Acumos software file is distributed by Fraunhofer Gesellschaft
# under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# This file is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ===============LICENSE_END===================================================
import os
from pathlib import Path

from utils import APIClient

NEXUS_REPO_NAME = os.environ['NEXUS_REPO_NAME']
NEXUS_ROOT_GROUP_ID = os.environ['NEXUS_ROOT_GROUP_ID']


class DocumentManager(object):
    def __init__(self):
        """
        This class manages the document storage and retrieval to the specified nexus host.
        It reads a lot of authentication related details from environment variables, make sure to set relevant details.
        The required environment variables are:
        - NEXUS_RW_USERNAME
        - NEXUS_RW_PASSWORD
        - NEXUS_HOST
        - NEXUS_PORT
        - NEXUS_REPO_NAME
        - NEXUS_ROOT_GROUP_ID
        """
        self.NEXUS_RW_USERNAME = os.environ['NEXUS_RW_USERNAME']
        self.NEXUS_RW_PASSWORD = os.environ['NEXUS_RW_PASSWORD']
        self.NEXUS_HOST = os.environ['NEXUS_HOST']
        self.NEXUS_PORT = os.environ['NEXUS_PORT']
        self.NEXUS_REPO_NAME = os.environ['NEXUS_REPO_NAME']
        self.NEXUS_ROOT_GROUP_ID = os.environ['NEXUS_ROOT_GROUP_ID']
        self.auth = (self.NEXUS_RW_USERNAME, self.NEXUS_RW_PASSWORD)
        self.host = f"http://{self.NEXUS_HOST}:{self.NEXUS_PORT}"
        self.api_client = APIClient(self.NEXUS_RW_USERNAME, self.NEXUS_RW_PASSWORD, self.host)

    def upload_document(self,
                        solution_id: str,
                        doc_name: str,
                        doc_location: str,
                        version: str = '1.0.0') -> str:
        """
        Uploads document to the nexus repository as an artifact.
        :param solution_id: indicates the current document belongs to the provided solution id.
        :param doc_name: name of the document, along with its extension.
        :param doc_location: location of the document in the current file system.
        :param version: document version as a string. Default value `1.0.0`
        :returns: url to the uploaded document.
        """
        params = (
            ('repository', self.NEXUS_REPO_NAME),
        )

        doc_ext = doc_name.split('.')[-1]
        doc_name_wo_ext = Path(doc_name).resolve().stem

        files = {
            # group id defines the root folder
            # in our case, it is the solution_id
            'maven2.groupId': (None, f'{self.NEXUS_ROOT_GROUP_ID}.{solution_id}'),
            'maven2.artifactId': (None, doc_name_wo_ext),
            'maven2.version': (None, version),
            'maven2.asset1': (doc_name, open(doc_location, 'rb')),
            'maven2.asset1.extension': (None, doc_ext),
        }

        response = self.api_client.post_document_request("service/rest/beta/components", params, files)
        if response.status_code in [i for i in range(200, 210)]:
            api_target = self.build_artifact_dwn_link(solution_id, doc_name_wo_ext, version, doc_ext, NEXUS_REPO_NAME,
                                                      NEXUS_ROOT_GROUP_ID)
            return api_target
        return None

    def build_artifact_dwn_link(self,
                                solution_id: str,
                                doc_name_wo_ext: str,
                                version: str,
                                doc_ext: str,
                                repo_name: str = NEXUS_REPO_NAME,
                                group_id: str = NEXUS_ROOT_GROUP_ID):
        """
        Builds the url to the document uploaded to the nexus repository.
        :param solution_id: indicates the current document belongs to the provided solution id.
        :param doc_name_wo_ext: name of the document, without its extension.
        :param version: document version as a string.
        :param doc_ext: file extension of the document.
        :param repo_name: name of the repository, where the document is stored in nexus system.
        :param group_id: group id is the group identifier, under which we store the component's artifacts.
        :returns: url to the uploaded document.
        """
        group_id_link = group_id.replace('.', '/')
        api_url = f'repository/{repo_name}/{group_id_link}/{solution_id}/{doc_name_wo_ext}/{version}/{doc_name_wo_ext}-{version}.{doc_ext}'
        return self.api_client.build_url(api_url)

    def retrieve_document(self, api_url: str):
        """
        Retrieves document from the nexus repository.
        :param api_url: url at which the document is stored in the nexus repository.
        :returns: a dictionary object containing the contents of downloaded document.
        """
        response = self.api_client.get_document(api_url)
        return response
    
    def get_document(self, api_url):
        """
        Retrieves document from the nexus repository, when we have the direct link from artifact URI.
        :param api_url: which is a partial artifact URI at which the document is stored in the nexus repository.
        :returns: a dictionary object containing the contents of downloaded document.
        """
        new_url=f"repository/{self.NEXUS_REPO_NAME}/{api_url}"
        response = self.api_client.get_document(new_url)
        return response

if __name__ == "__main__":
    doc_name = 'sample_tutorial_solutions.json'
    solution_id = 'sample-tutorial-solution'
    location = 'tutorial_solutions.json'
    version = '1.0.0'
    dh = DocumentManager()
    document_link = dh.upload_document(solution_id, doc_name, location, version)
    ret_doc_dict = dh.retrieve_document(document_link)
    ret_doc_dict
